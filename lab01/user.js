const fs = require("fs")
const lodash = require('lodash');
const {toJSON} = require("lodash/seq");

const list = () => {
    const data = fs.readFileSync('user.json', 'utf8')
    const user = JSON.parse(data)
    return user.languages;
}

function add(language) {
    //Додавання нової мови
    const data = fs.readFileSync('user.json', 'utf8')
    const user = JSON.parse(data)
    user.languages.push(language);
    fs.writeFileSync('user.json', JSON.stringify(user));
}

function remove(language) {
    //Видалення мови
    const data = fs.readFileSync('user.json', 'utf8')
    const user = JSON.parse(data)
    lodash.remove(user.languages, language);
    fs.writeFileSync('user.json', JSON.stringify(user));
}

function read(language) {
    //Повернути інформацію про мову
    const data = fs.readFileSync('user.json', 'utf8')
    const user = JSON.parse(data)
    const languageIndex = lodash.findIndex(user.languages, language)
    return user.languages[languageIndex];
}

module.exports = {list, add, remove, read}