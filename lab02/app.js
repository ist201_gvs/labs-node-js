const express = require("express");
let app = express();
const hbs = require("hbs");
const axios = require("axios");

hbs.registerPartials(__dirname + '/views/partials')

app.set('view engine', 'hbs');

app.get('/', (req, res) => {
    res.render('../views/index.hbs')
});

app.get('/weather/:city', (req, res) => {
    const weather = {
        description: "Clear sky"
    }
    const key = "a66092bda7c246593e895ef7d87995cb";
    const city = req.params.city;
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}`;
    axios.post(url)
        .then((response) => {
            console.log(response.data);
            const data = response.data;
            res.render('../views/weather.hbs', {data})
        })
        .catch((error) => {
            console.log(error);
        })
})

app.get('/login', (req, res) => {
    res.send("This is the login page");
})

app.listen(3000, () => {
    console.log("Example app listening on port 3000")
})
