const {User} = require("./models/user");
const {Task} = require("./models/task")
require('./db/mongoose')
require('./models/user')
require('./middleware/auth')
const express = require('express')
let app = express();
const bodyParser = require('body-parser')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Отримати всіх користувачів
app.get("/users", (req, res) => {
   User.find({}).then((users) => {
      res.status(200).send(users);
   }).catch((error) => {
      res.status(500).send(error);
   });
});

// Отримати користувача по ID
app.get("/users/:id", function (req, res) {
    let id = req.params["id"];
    User.find({"_id" : id}).then((users) => {
        res.status(201).send(users);
    }).catch((error) => {
        res.status(500).send(error);
    });
});

// Додати нового користувача
app.post('/add', async (req, res)  => {
    let user = new User(req.body);
    const token = await user.generateAuthToken();
    await user.save().then(() => {
        res.status(201).send("User added!\n" + user)
    }). catch((error) => {
        res.status(500).send(error)
    });
})

// Видалити користувача по ID
app.get("/user/:id", async (req, res) => {
    let id = req.params["id"];
    await User.deleteOne({"_id" : id}).then(() => {
        res.status(201).send("Success(200)");
    }).catch(() => {
        res.status(500).send("Error(400)");
    });
});

// Оновити користувача по ID
app.put("/user/update/:id",  async (req, res) => {
    const user = await User.findById(req.params["id"]);
    const updates = ['name', 'age', 'email', 'password'];
    updates.forEach((update) => user[update] = req.body[update]);
    await user.save().then(() => {
        res.status(200).send("User Updated !");
    }).catch((error) => {
        res.status(500).send(error);
    });
});

// Отримати Task по ID
app.get("/tasks/:id", function (request, response) {
    let id = request.params["id"];
    Task.find({"_id" : id}).then((tasks) => {
        response.status(201).send(tasks);
    }).catch((error) => {
        response.status(500).send(error);
    });
});

// Отримати всі Task
app.get("/tasks", (req, res) => {
    Task.find({}).then((users) => {
        res.status(200).send(users);
    }).catch((error) => {
        res.status(500).send(error);
    });
});

// Додати Task
app.post('/tasks/add', async (req, res)  => {
    let task = new Task(req.body)
    await task.save().then(() => {
        res.status(201).send("Task added!\n" + task)
    }). catch((error) => {
        res.status(500).send(error)
    });
})

// Видалити Task по ID
app.get("/task/:id", async (req, res) => {
    let id = req.params["id"];
    await Task.deleteOne({"_id" : id}).then(() => {
        res.status(201).send("Deleted !");
    }).catch((error) => {
        res.status(500).send(error);
    });
});

// Оновити Task по ID
app.put("/task/update/:id",  async (req, res) => {
    let id = req.params["id"];
    const params = {};
    if (req.body.description) params.description = req.body.description;
    if (req.body.completed) params.completed = req.body.completed;
    await Task.updateOne({"_id" : id}, params, {new : true}).then(() => {
        res.status(200).send("Task Updated !");
    }).catch((error) => {
        res.status(500).send(error);
    });
});

// Not Found 404
app.use(function(req, res) {
    res.status(404).end('Not Found 404');
});

app.listen(3000, function () {
    console.log('listening on *:3000');
});
