const express = require('express');
require('./../db/mongoose')
const router = new express.Router();
const auth = require("../middleware/auth");
const bodyParser = require("body-parser");
const {User} = require("../models/user");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));



router.get('/test', (req, res) => {
    res.send("From a new File");
})

router.post("/users/login", async (req, res) => {
    try {
        const user = await User.findOneByCredentials(User, req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({user, token})
    } catch (e) {
        res.status(400).send()
    }
})

router.get('/users/me', auth, async (req, res) => {
    res.send(req.user);
})

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.to.filter((token) => {
            return token.token !== req.token;
        })
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router;