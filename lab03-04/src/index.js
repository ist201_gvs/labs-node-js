const express = require('express')
let app = express();
require('./db/mongoose')
require('./middleware/auth')
const userRouter = require('./routers/user')


app.use(userRouter);

app.listen(3000, function () {
    console.log('listening on *:3000');
});


