const mongoose = require("mongoose");

module.exports.Task = mongoose.model('Task', {
    description:
        {
            type: String,
            required: true,
            trim: true
        },
    completed:
        {
            type: Boolean,
            required: false,
            default: false,
        },
});